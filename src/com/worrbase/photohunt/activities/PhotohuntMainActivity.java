package com.worrbase.photohunt.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.worrbase.photohunt.util.DBHelper;
import com.worrbase.photohunt.adapters.MainGridAdapter;
import com.worrbase.photohunt.R;
import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.InputStream;
import java.security.KeyStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotohuntMainActivity extends Activity {
    public static final String LOGNAME = "Photohunt";
    public static final String BASE_URI = "https://photohunt.csh.rit.edu/api";
    public static final String CONTENT_TYPE = "application/json";
    public static final String TOKEN_PREF = "token";
    public static final String GAMEID_PREF = "gameid";
    public static final String TEAMNAME_PREF = "team";
    public static final String START_PREF = "startTime";
    public static final String END_PREF = "endTime";
    public static final String MAX_PREF = "maxPhotos";
    public static final String MAX_JUDGE_PREF = "maxJudgedPhotos";
    public static final String PREFS_NAME = "Photohunt";
    public static final AsyncHttpClient HTTP_CLIENT = new AsyncHttpClient();

    // Looks like we need an SSLSocketFactoryFactory
    private SSLSocketFactory createSSLSocketFactory() {
        try {
            KeyStore ks = KeyStore.getInstance("BKS");
            InputStream in = getResources().openRawResource(R.raw.store);
            ks.load(in, "Photohunt!".toCharArray());
            in.close();
            return new SSLSocketFactory(ks);
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not init keystore");
        }

        return null;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        HTTP_CLIENT.setSSLSocketFactory(createSSLSocketFactory());
        HTTP_CLIENT.addHeader("accept", CONTENT_TYPE);

        // If we're not logged in, redirect to Login Activity
        SharedPreferences sp = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (sp.getString(TOKEN_PREF, "").equals("")) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }

        // Set the size of images based on screen size
        // getWidth and getHeight are deprecated, but are the best way to support old Android
        Display display = getWindowManager().getDefaultDisplay();
        GalleryActivity.setThumbHeight(display.getWidth() / 3);
        GalleryActivity.setThumbWidth(display.getWidth() / 3);

        setContentView(R.layout.main);
        GridView gridView = (GridView)findViewById(R.id.main_grid);
        gridView.setAdapter(new MainGridAdapter(this));

        TextView teamNameTextView = (TextView)findViewById(R.id.team_name);
        TextView timeLeftTextView = (TextView)findViewById(R.id.time_left);
        TextView progressTextView = (TextView)findViewById(R.id.progress);

        teamNameTextView.setText(sp.getString(TEAMNAME_PREF, ""));

        // Parse the end date given to us by the Photohunt server and find time left
        Date endDate;
        final Date now = new Date();
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            endDate = inputDateFormat.parse(sp.getString(END_PREF, ""));
            Date timeLeft = new Date(endDate.getTime() - now.getTime());
            timeLeftTextView.setText(outputDateFormat.format(timeLeft));
        } catch (ParseException e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not parse date");
            Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
        }

        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getReadableDatabase();
        long numJudged = DBHelper.numJudgedPhotos(db);

        final long numTaken = DBHelper.numPhotos(db);
        final long numAllowed = sp.getInt(PhotohuntMainActivity.MAX_PREF, -1);

        db.close();

        progressTextView.setText(""+ (numJudged >= 0 ? numJudged : 0) + "/" + sp.getInt(MAX_JUDGE_PREF, -1));

        if (display.getHeight() < display.getWidth()) {
            gridView.setColumnWidth(display.getHeight() / 3);
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sp = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

                Date startDate = now;
                Date endDate = now;
                SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
                try {
                    startDate = inputDateFormat.parse(sp.getString(START_PREF, ""));
                    endDate = inputDateFormat.parse(sp.getString(END_PREF, ""));
                } catch (ParseException e) {
                    Log.e(PhotohuntMainActivity.LOGNAME, "Could not parse date");
                    Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
                }

                switch (i) {
                    case 0:
                        // Camera
                        if (numTaken >= numAllowed) {
                            Toast.makeText(PhotohuntMainActivity.this, "Cannot take any more photos", Toast.LENGTH_SHORT).show();
                        } else if (now == startDate || now.before(startDate)) {
                            Toast.makeText(PhotohuntMainActivity.this, "Competition has not started yet", Toast.LENGTH_SHORT).show();
                        } else if (now == endDate || now.after(endDate)) {
                            Toast.makeText(PhotohuntMainActivity.this, "Competition has ended", Toast.LENGTH_SHORT).show();
                        } else {
                            startActivity(new Intent(PhotohuntMainActivity.this, PhotoCaptureActivity.class));
                        }

                        break;
                    case 1:
                        // Gallery
                        startActivity(new Intent(PhotohuntMainActivity.this, GalleryActivity.class));
                        break;
                    case 2:
                        // Clues
                        Intent intent = new Intent(PhotohuntMainActivity.this, TagsActivity.class);
                        intent.putExtra(TagsActivity.EDITABLE, false);
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
