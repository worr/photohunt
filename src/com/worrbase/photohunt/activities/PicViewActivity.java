package com.worrbase.photohunt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import com.worrbase.photohunt.R;

public class PicViewActivity extends PicActivity implements View.OnClickListener, BundleUp {
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    private GestureDetector gestureDetector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gestureDetector = new GestureDetector(new FlingGestureDetector());
        setContentView(R.layout.image);
        updateImage(current, R.id.pic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.image_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.notes:
                intent = new Intent(this, PicEditActivity.class);
                intent.putExtras(bundleUp());
                startActivity(intent);
                return true;
            case android.R.id.home:
                intent = new Intent(this, GalleryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void updateImage(int idx, int viewId) {
        super.updateImage(idx, viewId);

        ImageView imageView = (ImageView)findViewById(R.id.pic);
        imageView.setOnClickListener(this);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Log.d(PhotohuntMainActivity.LOGNAME, "onClick()");
        nextImage(R.id.pic);
    }

    private class FlingGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            Log.d(PhotohuntMainActivity.LOGNAME, "onFling()");

            if (motionEvent.getX() - motionEvent1.getX() > SWIPE_MIN_DISTANCE && Math.abs(v) > SWIPE_THRESHOLD_VELOCITY) {
                nextImage(R.id.pic);
                return true;
            }

            if (motionEvent1.getX() - motionEvent.getX() > SWIPE_MIN_DISTANCE && Math.abs(v) > SWIPE_THRESHOLD_VELOCITY) {
                prevImage(R.id.pic);
                return true;
            }

            return false;
        }

        // It is necessary to return true from onDown for the onFling event to register
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }
}
