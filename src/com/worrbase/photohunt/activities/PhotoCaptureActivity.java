package com.worrbase.photohunt.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import com.worrbase.photohunt.services.PhotoUploadService;
import com.worrbase.photohunt.util.DBHelper;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoCaptureActivity extends Activity {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    private static final String DIRNAME = ".photohunt";

    private static Uri getOutputMedia() {
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory(), DIRNAME);
        
        if (! mediaStorageDir.exists()) {
            if (! mediaStorageDir.mkdirs()) {
                Log.d(PhotohuntMainActivity.LOGNAME, "failed to create directory");
            }
        }
        
        return Uri.fromFile(new File(mediaStorageDir.getPath(),
            "CSH_tmp.jpg"));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File dir = getDir(getFilesDir().getName(), Context.MODE_PRIVATE);
        if (! dir.exists()) {
            if (! dir.mkdirs()) {
                Log.e(PhotohuntMainActivity.LOGNAME, "Could not make save directory");
            }
        }

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri pictureUri = getOutputMedia();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
        startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Toast.makeText(this, "Picture saved!", Toast.LENGTH_SHORT).show();
                if (intent == null) {
                    savePicture(getOutputMedia());
                } else {
                    savePicture(intent.getData());
                }

                finish();
                startActivity(new Intent(this, GalleryActivity.class));
            } else {
                finish();
                startActivity(new Intent(this, PhotohuntMainActivity.class));
            }
        }
    }

    private void savePicture(Uri image) {
        BufferedOutputStream fos;
        BufferedInputStream fis;
        File imageFile = new File(image.getPath());
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String basename = "CSH_" + timeStamp + ".jpg";
        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();

        if (!imageFile.getParentFile().exists()) {
            if (!imageFile.getParentFile().mkdirs()) {
                Log.e(PhotohuntMainActivity.LOGNAME, "SD card not writable");
                return;
            }
        }


        try {
            fos = new BufferedOutputStream(openFileOutput(basename, Context.MODE_PRIVATE));
            fis = new BufferedInputStream(new FileInputStream(imageFile));

            int i;
            while ((i = fis.read()) != -1)
                fos.write(i);
            fos.close();
            fis.close();

            DBHelper.addPhoto(db, getFilesDir() + "/" + basename);
            Intent intent = new Intent(this, PhotoUploadService.class);
            intent.putExtra(PhotoUploadService.PHOTO_ID, DBHelper.photoIdFromPath(db, getFilesDir() + "/" + basename));
            Log.d(PhotohuntMainActivity.LOGNAME, "ID is " + DBHelper.photoIdFromPath(db, getFilesDir() + "/" + basename));
            startService(intent);
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not create streams " + e.getMessage());
            Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
            Toast.makeText(this, "Could not save photo", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }
}
