package com.worrbase.photohunt.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.worrbase.photohunt.adapters.PicAdapter;
import com.worrbase.photohunt.R;

import android.support.v4.util.LruCache;

import java.io.File;

public class GalleryActivity extends Activity {
    private static int thumbWidth;
    private static int thumbHeight;
    private static int thumbScaleFactor = 0;

    public static int getThumbWidth() {
        return thumbWidth;
    }

    public static void setThumbWidth(int thumbWidth) {
        GalleryActivity.thumbWidth = thumbWidth;
    }

    public static int getThumbHeight() {
        return thumbHeight;
    }

    public static void setThumbHeight(int thumbHeight) {
        GalleryActivity.thumbHeight = thumbHeight;
    }

    public static int getThumbScaleFactor() {
        return thumbScaleFactor;
    }

    public static void setThumbScaleFactor(int thumbScaleFactor) {
        GalleryActivity.thumbScaleFactor = thumbScaleFactor;
    }

    // Let's make a thumbnail cache!
    private static final int THUMB_CACHE_SIZE = 10 * 1024 * 1024;
    public static final LruCache<File, Bitmap> thumbCache = new LruCache<File, Bitmap>(THUMB_CACHE_SIZE) {
        protected int sizeOf(File key, Bitmap value) {
            try {
                Bitmap.class.getMethod("getByteCount");
                return value.getByteCount();
            } catch (NoSuchMethodException e) {
                return (int)key.getAbsoluteFile().length();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.gallery);

        GridView gallery = (GridView)findViewById(R.id.gallery);
        gallery.setAdapter(new PicAdapter(this));

        try{
            this.getClass().getMethod("getActionBar");
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (NoSuchMethodException e) {
            Log.d(PhotohuntMainActivity.LOGNAME, "Your shit is old");
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not actionbar " + e.getMessage());
        }

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(GalleryActivity.this, PicViewActivity.class);
                Uri[] uris = new Uri[adapterView.getCount()];

                for (int j = 0; j < adapterView.getCount(); j++) {
                    uris[j] = (Uri)adapterView.getItemAtPosition(j);
                }

                bundle.putParcelableArray("uris", uris);
                bundle.putInt("current", i);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, PhotohuntMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
