package com.worrbase.photohunt.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.worrbase.photohunt.R;
import com.worrbase.photohunt.tasks.CluesTask;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class LoginActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);
        final Button b = (Button)findViewById(R.id.login_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Grab credentials from Login activity
                final String login = ((EditText)findViewById(R.id.login_text)).getText().toString();
                final String gameid = ((EditText)findViewById(R.id.gameid_text)).getText().toString();

                // Formulate request
                RequestParams params = new RequestParams();
                try {
                    params.put("token", URLEncoder.encode(login, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(LoginActivity.this, "Could not authenticate", Toast.LENGTH_SHORT).show();
                    Log.e(PhotohuntMainActivity.LOGNAME, "Could not authenticate: " + e.getMessage());
                }

                try{
                    params.put("gameid", URLEncoder.encode(login, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(LoginActivity.this, "Could not authenticate", Toast.LENGTH_SHORT).show();
                    Log.e(PhotohuntMainActivity.LOGNAME, "Could not authenticate" + e.getMessage());
                }

                // Log in and store preferences on success
                PhotohuntMainActivity.HTTP_CLIENT.get(PhotohuntMainActivity.BASE_URI + "/info",
                        params,
                        new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                Log.d(PhotohuntMainActivity.LOGNAME, response.toString());
                                try {
                                    SharedPreferences.Editor prefs = getSharedPreferences(PhotohuntMainActivity.PREFS_NAME, Context.MODE_PRIVATE).edit();
                                    response = response.getJSONObject("data");

                                    prefs.putString(PhotohuntMainActivity.TEAMNAME_PREF, response.getString(PhotohuntMainActivity.TEAMNAME_PREF));
                                    prefs.putString(PhotohuntMainActivity.START_PREF, response.getString(PhotohuntMainActivity.START_PREF));
                                    prefs.putString(PhotohuntMainActivity.END_PREF, response.getString(PhotohuntMainActivity.END_PREF));
                                    prefs.putInt(PhotohuntMainActivity.MAX_PREF, response.getInt(PhotohuntMainActivity.MAX_PREF));
                                    prefs.putInt(PhotohuntMainActivity.MAX_JUDGE_PREF, response.getInt(PhotohuntMainActivity.MAX_JUDGE_PREF));
                                    prefs.putString(PhotohuntMainActivity.TOKEN_PREF, login);
                                    prefs.putString(PhotohuntMainActivity.GAMEID_PREF, gameid);

                                    prefs.apply();

                                } catch (JSONException e) {
                                    Log.d(PhotohuntMainActivity.LOGNAME, "JSON parsing error: " + e.getMessage());
                                }
                                CluesTask ct = new CluesTask(LoginActivity.this);
                                ct.execute();

                                // Start main activity
                                Intent intent = new Intent(LoginActivity.this, PhotohuntMainActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onFailure(Throwable e, String res) {
                                Toast.makeText(LoginActivity.this, "Could not authenticate", Toast.LENGTH_SHORT).show();

                                Log.e(PhotohuntMainActivity.LOGNAME, "Could not authenticate: " + e.getMessage());
                                Log.e(PhotohuntMainActivity.LOGNAME, "Exception class: " + e.getClass().toString());
                                Log.e(PhotohuntMainActivity.LOGNAME, "Message: " + e.getMessage());
                                Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
                            }
                        });
            }
        });
    }
}
