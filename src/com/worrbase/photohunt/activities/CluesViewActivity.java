package com.worrbase.photohunt.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.worrbase.photohunt.util.DBHelper;

public class CluesViewActivity extends CluesActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListAdapter listAdapter =
                new SimpleCursorAdapter(this,
                        android.R.layout.two_line_list_item,
                        cursor,
                        new String[] { "text", "points" },
                        new int[] { android.R.id.text1, android.R.id.text2});
        setListAdapter(listAdapter);
        db.close();

        if (!bonuses) {
            ListView listView = getListView();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    cursor.moveToPosition(i);
                    DBHelper dbh = new DBHelper(CluesViewActivity.this);
                    SQLiteDatabase db = dbh.getReadableDatabase();
                    long id = cursor.getLong(0);
                    boolean hasBonuses = DBHelper.hasBonuses(db, id);
                    db.close();

                    if (hasBonuses) {
                        Intent intent = new Intent(CluesViewActivity.this, CluesViewActivity.class);
                        intent.putExtra(CluesActivity.CLUE_ID, id);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        this.onCreate(null);
    }
}
