package com.worrbase.photohunt.activities;

import android.R;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import com.worrbase.photohunt.util.DBHelper;

public class TagsActivity extends ListActivity {
    private Cursor cursor;
    private boolean editable;
    public static final String EDITABLE = "editable";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getReadableDatabase();

        editable = getIntent().getExtras().getBoolean(EDITABLE);

        cursor = DBHelper.getTagsCursor(db);
        startManagingCursor(cursor);
        ListAdapter listAdapter =
                new SimpleCursorAdapter(this,
                        R.layout.simple_list_item_1,
                        cursor,
                        new String[] { "tag" },
                        new int[] { R.id.text1 });

        setListAdapter(listAdapter);
        db.close();

        try{
            this.getClass().getMethod("getActionBar");
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (NoSuchMethodException e) {
            Log.d(PhotohuntMainActivity.LOGNAME, "Your shit is old");
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not actionbar " + e.getMessage());
        }

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cursor.moveToPosition(i);
                Intent intent;

                if (!editable)
                    intent = new Intent(TagsActivity.this, CluesViewActivity.class);
                else {
                    intent = new Intent(TagsActivity.this, CluesSelectActivity.class);
                    intent.putExtra(CluesSelectActivity.PHOTO_ID, getIntent().getExtras().getLong(CluesSelectActivity.PHOTO_ID));
                }

                intent.putExtra(CluesActivity.TAG_NAME, cursor.getString(1));
                startActivity(intent);
            }
        });
    }

    // TODO: Move init code to onStart()
    @Override
    public void onStart() {
        super.onStart();
        this.onCreate(null); // This is probably a bad thing to do...
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent;
                if (!editable)
                    intent = new Intent(this, PhotohuntMainActivity.class);
                else
                    intent = new Intent(this, GalleryActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
