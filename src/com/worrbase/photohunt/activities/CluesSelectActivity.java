package com.worrbase.photohunt.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import android.R;
import com.worrbase.photohunt.util.DBHelper;

import java.util.ArrayList;

public class CluesSelectActivity extends CluesActivity {
    private long photoId;
    public final static String PHOTO_ID = "photoid";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        photoId = getIntent().getExtras().getLong(PHOTO_ID);
        final ListView listView = getListView();
        ArrayList<Long> checked;

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        ListAdapter listAdapter =
                new SimpleCursorAdapter(this,
                        R.layout.select_dialog_multichoice,
                        cursor,
                        new String[] { "text", "points" },
                        new int[] { R.id.text1, R.id.text2 });
        setListAdapter(listAdapter);

        if (bonuses) {
            checked = DBHelper.checkedBonuses(db, photoId);
        } else {
            checked = DBHelper.checkedClues(db, photoId);
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Clear original status
            listView.setItemChecked(cursor.getPosition(), false);
            long id = cursor.getLong(0);
            if (checked.contains(id)) {
                listView.setItemChecked(cursor.getPosition(), true);
            }
            cursor.moveToNext();
        }

        db.close();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cursor.moveToPosition(i);

                DBHelper dbh = new DBHelper(CluesSelectActivity.this);
                SQLiteDatabase db = dbh.getReadableDatabase();
                boolean hasBonuses = false;
                long id = -1;

                if (!bonuses) {
                    id = cursor.getLong(0);
                    hasBonuses = DBHelper.hasBonuses(db, id);
                }

                if (listView.isItemChecked(i)) {
                    if (bonuses)
                        DBHelper.addBonusToPhoto(db, photoId, cursor.getLong(0));
                    else
                        DBHelper.addClueToPhoto(db, photoId, cursor.getLong(0));
                } else {
                    if (bonuses)
                        DBHelper.deleteBonusFromPhoto(db, photoId, cursor.getLong(0));
                    else
                        DBHelper.deleteClueFromPhoto(db, photoId, cursor.getLong(0));
                }

                db.close();

                if (!bonuses && hasBonuses && listView.isItemChecked(i)) {
                    Intent intent = new Intent(CluesSelectActivity.this, CluesSelectActivity.class);
                    intent.putExtra(CluesActivity.CLUE_ID, id);
                    intent.putExtra(CluesSelectActivity.PHOTO_ID, photoId);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        this.onCreate(null);
    }
}
