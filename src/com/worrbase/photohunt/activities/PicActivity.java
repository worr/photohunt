package com.worrbase.photohunt.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;

public class PicActivity extends Activity implements BundleUp {
    protected Parcelable[] uris;
    protected int current;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            Log.d(PhotohuntMainActivity.LOGNAME, "Bundle is empty");
            return;
        }

        uris = bundle.getParcelableArray("uris");
        current = bundle.getInt("current");

        try{
            this.getClass().getMethod("getActionBar");
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (NoSuchMethodException e) {
            Log.d(PhotohuntMainActivity.LOGNAME, "Your shit is old");
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not actionbar " + e.getMessage());
        }
    }

    protected void updateImage(int idx, int viewId) {
        BitmapFactory.Options bfo = new BitmapFactory.Options();
        bfo.inSampleSize = 4;

        if (idx == -1) idx = uris.length - 1;

        File pic = new File(((Uri)uris[idx % uris.length]).getPath());
        ImageView imageView = (ImageView)findViewById(viewId);
        try {
            imageView.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(pic), null, bfo));
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, e.getMessage());
        }

        current = idx % uris.length;
    }

    protected void nextImage(int viewId) {
        updateImage(current + 1, viewId);
    }

    protected void prevImage(int viewId) {
        updateImage(current - 1, viewId);
    }

    @Override
    public Bundle bundleUp() {
        Bundle ret = new Bundle();

        ret.putParcelableArray("uris", uris);
        ret.putInt("current", current);

        return ret;
    }
}
