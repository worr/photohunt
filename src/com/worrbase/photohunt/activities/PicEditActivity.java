package com.worrbase.photohunt.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import com.worrbase.photohunt.services.PhotoUploadService;
import com.worrbase.photohunt.util.DBHelper;
import com.worrbase.photohunt.R;

public class PicEditActivity extends PicActivity implements BundleUp {
    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getReadableDatabase();
        final long photoid = DBHelper.photoIdFromPath(db, ((Uri)uris[current]).getPath());

        setContentView(R.layout.image_comment);
        updateImage(current, R.id.small_pic);

        EditText notes = (EditText)findViewById(R.id.notes);
        notes.setText(DBHelper.getNotes(db, photoid));

        CheckBox judged = (CheckBox)findViewById(R.id.judge);
        judged.setChecked(DBHelper.isJudged(db, photoid));
        db.close();

        Button clues = (Button)findViewById(R.id.clues);
        clues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PicEditActivity.this, TagsActivity.class);
                intent.putExtra(TagsActivity.EDITABLE, true);
                intent.putExtra(CluesSelectActivity.PHOTO_ID, photoid);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                saveInformation();

                Intent intent = new Intent(this, PicViewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundleUp());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveInformation() {
        EditText notes = (EditText)findViewById(R.id.notes);
        CheckBox judge = (CheckBox)findViewById(R.id.judge);

        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();
        long photoid = DBHelper.photoIdFromPath(db, ((Uri)uris[current]).getPath());

        DBHelper.modifyNotes(db, photoid, notes.getText().toString());
        DBHelper.judgePhoto(db, photoid, judge.isChecked());
        db.close();

        Intent intent = new Intent(this, PhotoUploadService.class);
        intent.putExtra(PhotoUploadService.PHOTO_ID, photoid);
        startService(intent);
    }

    @Override
    public void onDestroy() {
        saveInformation();
        super.onDestroy();
    }
}