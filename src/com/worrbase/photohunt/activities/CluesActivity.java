package com.worrbase.photohunt.activities;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.worrbase.photohunt.util.DBHelper;

public class CluesActivity extends ListActivity {
    protected Cursor cursor;
    protected SQLiteDatabase db;
    protected boolean bonuses;

    public static final String TAG_NAME = "tagname";
    public static final String CLUE_ID = "cludid";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBHelper dbh = new DBHelper(this);
        db = dbh.getReadableDatabase();

        String tag = null;
        long clueId = -1;
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            tag = intent.getExtras().getString(TAG_NAME);
            clueId = intent.getExtras().getLong(CLUE_ID);
        }

        bonuses = false;
        if (tag != null) {
            cursor = DBHelper.getCluesCursor(db, tag);
        } else if (clueId != -1) {
            cursor = DBHelper.getCluesCursor(db, clueId);
            bonuses = true;
        } else {
            cursor = DBHelper.getCluesCursor(db);
        }

        startManagingCursor(cursor);

        try{
            this.getClass().getMethod("getActionBar");
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (NoSuchMethodException e) {
            Log.d(PhotohuntMainActivity.LOGNAME, "Your shit is old");
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not actionbar " + e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (bonuses) {
                    finish();
                    return true;
                }

                Intent intent = new Intent(this, TagsActivity.class);
                if (this instanceof CluesViewActivity)
                    intent.putExtra(TagsActivity.EDITABLE, false);
                if (this instanceof CluesSelectActivity)
                    intent.putExtra(TagsActivity.EDITABLE, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        cursor.close();
        super.onDestroy();
    }
}
