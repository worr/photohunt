package com.worrbase.photohunt.tasks;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.worrbase.photohunt.util.DBHelper;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class CluesTask extends AsyncTask<String, Boolean, Boolean> {
    WeakReference<Activity> activityWeakReference;

    public CluesTask(Activity activity) {
        activityWeakReference = new WeakReference<Activity>(activity);
    }

    protected Boolean doInBackground(String... asyncParams) {
        SharedPreferences prefs = activityWeakReference.get().getSharedPreferences(PhotohuntMainActivity.PREFS_NAME, Context.MODE_PRIVATE);
        RequestParams params = new RequestParams("token", prefs.getString(PhotohuntMainActivity.TOKEN_PREF, ""));
        PhotohuntMainActivity.HTTP_CLIENT.get(
                PhotohuntMainActivity.BASE_URI + "/clues",
                params,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            JSONArray clues = response.getJSONArray("data");
                            for (int i = 0; i < clues.length(); i++) {
                                JSONObject clue = clues.getJSONObject(i);
                                long id = clue.getLong("id");
                                String desc = clue.getString("description");
                                long points = clue.getLong("points");

                                Log.d(PhotohuntMainActivity.LOGNAME, "adding: " + desc);

                                String[] bonusNames = null;
                                long[] bonusIds = null;
                                long[] bonusPoints = null;
                                if (clue.has("bonuses")) {
                                    bonusNames = new String[clue.getJSONArray("bonuses").length()];
                                    bonusIds = new long[clue.getJSONArray("bonuses").length()];
                                    bonusPoints = new long[clue.getJSONArray("bonuses").length()];
                                    for (int j = 0; j < clue.getJSONArray("bonuses").length(); j++) {
                                        bonusNames[j] = clue.getJSONArray("bonuses").getJSONObject(j).getString("description");
                                        bonusIds[j] = clue.getJSONArray("bonuses").getJSONObject(j).getLong("id");
                                        bonusPoints[j] = clue.getJSONArray("bonuses").getJSONObject(j).getLong("points");
                                    }
                                }

                                String[] tags = new String[clue.getJSONArray("tags").length()];
                                for (int j = 0; j < clue.getJSONArray("tags").length(); j++) {
                                    tags[j] = clue.getJSONArray("tags").getString(j);
                                }

                                // TODO: optimize db calls
                                DBHelper dbh = new DBHelper(activityWeakReference.get());
                                SQLiteDatabase db = dbh.getWritableDatabase();
                                if (bonusNames.length == 0) {
                                    DBHelper.addClue(db, desc, id, points, tags);
                                } else {
                                    DBHelper.addClue(db, desc, id, points, tags, bonusNames, bonusIds, bonusPoints);
                                }
                                db.close();
                            }
                        } catch (JSONException e) {
                            Log.e(PhotohuntMainActivity.LOGNAME, "Error parsing clues");
                            Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
                        }
                    }
                });

        return true;
    }
}
