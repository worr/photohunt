package com.worrbase.photohunt.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;
import com.worrbase.photohunt.activities.GalleryActivity;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class GalleryTask extends AsyncTask<File, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewWeakReference;

    public GalleryTask(ImageView imageView) {
        super();
        imageViewWeakReference = new WeakReference<ImageView>(imageView);
    }

    @Override
    protected Bitmap doInBackground(File... files) {
        synchronized (GalleryActivity.thumbCache) {
            if (GalleryActivity.thumbCache.get(files[0]) != null)
                return GalleryActivity.thumbCache.get(files[0]);
        }

        try {
            ExifInterface exifInterface = new ExifInterface(files[0].getAbsolutePath());
            if (exifInterface.hasThumbnail()) {
                BitmapFactory.Options bfo = new BitmapFactory.Options();
                bfo.inSampleSize = 2; // My testing says two is ideal
                Bitmap bitmap = BitmapFactory.decodeByteArray(exifInterface.getThumbnail(), 0, exifInterface.getThumbnail().length, bfo);
                GalleryActivity.thumbCache.put(files[0], bitmap);

                return bitmap;
            } else {
                BitmapFactory.Options bfo = new BitmapFactory.Options();
                Bitmap bitmap;

                // Calculate optimal scaling level if not calculated yet
                if (GalleryActivity.getThumbScaleFactor() == 0) {
                    bfo.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(new FileInputStream(files[0]), null, bfo);
                    int sampleSize = 8; // 8 seems like a good default
                    if (bfo.outHeight > GalleryActivity.getThumbHeight() || bfo.outWidth > GalleryActivity.getThumbWidth()) {
                        if (GalleryActivity.getThumbWidth() > GalleryActivity.getThumbHeight()) {
                            sampleSize = Math.round((float)GalleryActivity.getThumbHeight() / (float)bfo.outHeight);
                        } else {
                            sampleSize = Math.round(((float)GalleryActivity.getThumbWidth() / (float)bfo.outWidth));
                        }
                    }

                    GalleryActivity.setThumbScaleFactor(sampleSize);
                }

                bfo = new BitmapFactory.Options();
                bfo.inSampleSize = GalleryActivity.getThumbScaleFactor();
                bitmap = BitmapFactory.decodeStream(new FileInputStream(files[0]), null, bfo);
                try {
                    GalleryActivity.thumbCache.put(files[0], bitmap);
                } catch (NullPointerException e) {
                    Log.d(PhotohuntMainActivity.LOGNAME, "Simubug");
                }

                return bitmap;
            }
        } catch (IOException e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not load thumbnail: " + e.getMessage());
            Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            return;
        }

        if (imageViewWeakReference != null && bitmap != null) {
            final ImageView ref = imageViewWeakReference.get();
            if (ref != null) {
                ref.setImageBitmap(bitmap);
                ref.setLayoutParams(new GridView.LayoutParams(GalleryActivity.getThumbHeight(), GalleryActivity.getThumbWidth()));
                ref.setScaleType(ImageView.ScaleType.CENTER_CROP);

                Log.d(PhotohuntMainActivity.LOGNAME, "Thumbnail cache hits: " + GalleryActivity.thumbCache.hitCount());
            }
        }
    }
}
