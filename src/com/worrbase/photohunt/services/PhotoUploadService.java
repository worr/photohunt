package com.worrbase.photohunt.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;
import com.worrbase.photohunt.util.DBHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PhotoUploadService extends IntentService {
    public static final String PHOTO_ID = "photoid";
    private final String CLUES = "clues";
    private final String ID = "id";
    private final String BONUS_ID = "bonus_id";
    private final String JUDGE = "judge";
    private final String NOTES = "notes";

    public PhotoUploadService() {
        super("upload service");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        final long photoid = intent.getLongExtra(PHOTO_ID, -1);
        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getReadableDatabase();
        Log.d(PhotohuntMainActivity.LOGNAME, "Starting service...");

        Cursor photoCursor = DBHelper.getPhoto(db, photoid);
        Cursor cluesCursor = DBHelper.getCluesCursorForPhoto(db, photoid);

        photoCursor.moveToFirst();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(CLUES, new JSONArray());
            jsonObject.put(JUDGE, photoCursor.getInt(0) != 0);
            jsonObject.put(NOTES, photoCursor.getString(1));

            cluesCursor.moveToFirst();
            while (!cluesCursor.isAfterLast()) {
                int position = cluesCursor.getPosition();
                jsonObject.getJSONArray(CLUES).put(new JSONObject());
                jsonObject.getJSONArray(CLUES).getJSONObject(position).put(ID, cluesCursor.getLong(0));
                jsonObject.getJSONArray(CLUES).getJSONObject(position).put(BONUS_ID, new JSONArray(DBHelper.getBonusCursorForPhoto(db, photoid, cluesCursor.getLong(0))));
                cluesCursor.moveToNext();
            }
        } catch (JSONException e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not parse JSON: " + e.getMessage());
        }

        Log.d(PhotohuntMainActivity.LOGNAME, "JSON string: " + jsonObject.toString());
        RequestParams requestParams = new RequestParams();
        SharedPreferences sp = getSharedPreferences(PhotohuntMainActivity.PREFS_NAME, Context.MODE_PRIVATE);
        try {
            File file = new File(photoCursor.getString(2));
            requestParams.put("photo", new FileInputStream(file), file.getName(), "image/jpeg");
        } catch (FileNotFoundException e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not find file: " + e.getMessage());
        }
        requestParams.put("json", jsonObject.toString());
        String token = sp.getString(PhotohuntMainActivity.TOKEN_PREF, "");
        PhotohuntMainActivity.HTTP_CLIENT.post(PhotohuntMainActivity.BASE_URI + "/photos/new?token=" + token, requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable e, String res) {
                Log.e(PhotohuntMainActivity.LOGNAME, res);
                Log.e(PhotohuntMainActivity.LOGNAME, Log.getStackTraceString(e));
                Intent intent = new Intent(PhotoUploadService.this, PhotoUploadService.class);
                intent.putExtra(PHOTO_ID, photoid);
                startService(intent);
            }
        });

        cluesCursor.close();
        photoCursor.close();
        db.close();
        db = dbh.getWritableDatabase();
        DBHelper.uploadPhoto(db, photoid);
        db.close();
    }
}
