package com.worrbase.photohunt.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.worrbase.photohunt.R;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MainGridAdapter extends BaseAdapter {
    private Context context;
    private static final int CAMERA_THUMB = R.drawable.camera;
    private static final int CLUES_THUMB = R.drawable.clues;

    public MainGridAdapter(Context context) {
        this.context = context;
    }

    public int getCount() {
        return 3;
    }

    public Object getItem(int position) {
        switch (position) {
            case 0:
                return CAMERA_THUMB;
            case 1:
                File[] gallery = context.getFilesDir().listFiles();
                File image = gallery[(int)(Math.random() * gallery.length)];
                return Uri.fromFile(image);
            default:
                return null;
        }
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view;
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        BitmapFactory.Options bfo = new BitmapFactory.Options();
        bfo.inSampleSize = 8;

        int displayWidth = display.getWidth();
        int displayHeight = display.getHeight();

        int divisor = displayWidth < displayHeight ? 2 : 3;
        int width = displayWidth < displayHeight ? displayWidth : displayHeight;

        if (convertView == null) {
            view = new ImageView(context);
            view.setLayoutParams(new GridView.LayoutParams(width / divisor, width / divisor));
            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            view = (ImageView)convertView;
        }

        switch (position) {
            case 0:
                view.setImageResource(CAMERA_THUMB);
                break;
            case 1:
                File[] gallery = context.getFilesDir().listFiles();
                if (gallery.length == 0) {
                    view.setImageResource(CAMERA_THUMB);
                    return view;
                }
                File image = gallery[(int)(Math.random() * gallery.length)];
                try {
                    Log.d(PhotohuntMainActivity.LOGNAME, "Image is: " + image.getPath());
                    ExifInterface ei = new ExifInterface(image.getPath());
                    if (ei.hasThumbnail()) {
                        view.setImageBitmap(BitmapFactory.decodeByteArray(ei.getThumbnail(), 0, ei.getThumbnail().length));
                    } else {
                        view.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(image), null, bfo));
                    }
                } catch (FileNotFoundException e) {
                    Log.e(PhotohuntMainActivity.LOGNAME, "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(PhotohuntMainActivity.LOGNAME, "Could not read EXIF data: " + e.getMessage());
                }

                break;
            case 2:
                view.setImageResource(CLUES_THUMB);
                break;
        }

        view.setPadding(2, 2, 2, 2);
        return view;
    }
}
