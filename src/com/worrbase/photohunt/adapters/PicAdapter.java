package com.worrbase.photohunt.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.worrbase.photohunt.activities.GalleryActivity;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;
import com.worrbase.photohunt.tasks.GalleryTask;
import com.worrbase.photohunt.util.GalleryDrawable;

import java.io.File;

public class PicAdapter extends BaseAdapter {
    private File galleryRoot;
    private Context context;

    public PicAdapter(Context context) {
        this.context = context;
        galleryRoot = context.getFilesDir();
        Log.d(PhotohuntMainActivity.LOGNAME, "getFilesDir(): " + galleryRoot.getPath());
        if (!galleryRoot.exists()) {
            if (!galleryRoot.mkdirs()) {
                Log.e(PhotohuntMainActivity.LOGNAME, galleryRoot.toString());
                Log.e(PhotohuntMainActivity.LOGNAME, "Could not create storage");
            }
        }
    }
    
    public int getCount() {
        return galleryRoot.listFiles().length;
    }

    public Object getItem(int position) {
        return Uri.fromFile(galleryRoot.listFiles()[position]);
    }
    
    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);

        if (convertView != null) {
            if (convertView instanceof ImageView) {
                if (((ImageView)convertView).getDrawable() instanceof GalleryDrawable) {
                    imageView = (ImageView)convertView;

                    // Cancel running task
                    ((GalleryDrawable)imageView.getDrawable()).getGalleryTask().cancel(true);
                }
            }
        }

        GalleryTask task = new GalleryTask(imageView);
        imageView.setImageDrawable(new GalleryDrawable(context.getResources(), task));
        task.execute(galleryRoot.listFiles()[position]);
        imageView.setMinimumHeight(GalleryActivity.getThumbHeight());
        imageView.setMinimumWidth(GalleryActivity.getThumbWidth());
        return imageView;
    }
}
