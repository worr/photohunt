package com.worrbase.photohunt.util;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import com.worrbase.photohunt.tasks.GalleryTask;

import java.lang.ref.WeakReference;

public class GalleryDrawable extends BitmapDrawable {
    private WeakReference<GalleryTask> galleryTaskWeakReference;

    public GalleryDrawable(Resources res, GalleryTask task) {
        super(res);
        this.galleryTaskWeakReference = new WeakReference<GalleryTask>(task);
    }

    public GalleryTask getGalleryTask() {
        return galleryTaskWeakReference.get();
    }
}
