package com.worrbase.photohunt.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.worrbase.photohunt.activities.PhotohuntMainActivity;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "photohunt";

    // Clue table
    private static final String CLUE_TABLE_NAME = "clue";
    private static final String CLUE_ID = "_id";
    private static final String CLUE_TEXT = "text";
    private static final String CLUE_POINTS = "points";
    private static final String CLUE_SERVER_ID = "server_id";
    private static final String CREATE_CLUE_TABLE =
            "CREATE TABLE " + CLUE_TABLE_NAME + " (" +
            CLUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CLUE_TEXT + " TEXT, " +
            CLUE_POINTS + " INTEGER, " +
            CLUE_SERVER_ID + " INTEGER " +
            ");";

    // Bonus table
    private static final String BONUS_TABLE_NAME = "bonus";
    private static final String BONUS_ID = "_id";
    private static final String BONUS_TEXT = "text";
    private static final String BONUS_POINTS = "points";
    private static final String BONUS_SERVER_ID = "server_id";
    private static final String CREATE_BONUS_TABLE =
            "CREATE TABLE " + BONUS_TABLE_NAME + " (" +
            BONUS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            BONUS_TEXT + " TEXT, " +
            BONUS_POINTS + " INTEGER, " +
            BONUS_SERVER_ID + " INTEGER " +
            ");";

    // Clue to Bonus mapping table
    private static final String CB_TABLE_NAME = "cb";
    private static final String CB_CLUE_ID = "clue_id";
    private static final String CB_BONUS_ID = "bonus_id";
    private static final String CREATE_CB_TABLE =
            "CREATE TABLE " + CB_TABLE_NAME + " (" +
            CB_CLUE_ID + " INTEGER, " +
            CB_BONUS_ID + " INTEGER, " +
            "FOREIGN KEY(" + CB_CLUE_ID + ") REFERENCES " + CLUE_TABLE_NAME + "(" + CLUE_ID + ")," +
            "FOREIGN KEY(" + CB_BONUS_ID + ") REFERENCES " + BONUS_TABLE_NAME + "(" + BONUS_ID + ")" +
            ");";

    // Photo table
    private static final String PHOTO_TABLE_NAME = "photo";
    private static final String PHOTO_ID = "_id";
    private static final String PHOTO_PATH = "path";
    private static final String PHOTO_TIME = "time";
    private static final String PHOTO_MODIFIED = "mod";
    private static final String PHOTO_UPLOADED = "uploaded";
    private static final String PHOTO_NOTE = "note";
    private static final String PHOTO_JUDGE = "judge";
    private static final String CREATE_PHOTO_TABLE =
            "CREATE TABLE " + PHOTO_TABLE_NAME + " (" +
            PHOTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PHOTO_PATH + " TEXT UNIQUE NOT NULL, " +
            PHOTO_TIME + " INTEGER NOT NULL, " +
            PHOTO_MODIFIED + " INTEGER, " +
            PHOTO_UPLOADED + " INTEGER, " +
            PHOTO_NOTE + " TEXT, " +
            PHOTO_JUDGE + " INTEGER DEFAULT 1" +
            ");";


    // Photo to Clue mapping table
    private static final String PC_TABLE_NAME = "pc";
    private static final String PC_PHOTO_ID = "photo_id";
    private static final String PC_CLUE_ID = "clue_id";
    private static final String CREATE_PC_TABLE =
            "CREATE TABLE " + PC_TABLE_NAME + " (" +
            PC_PHOTO_ID + " INTEGER, " +
            PC_CLUE_ID + " INTEGER, " +
            "FOREIGN KEY(" + PC_PHOTO_ID + ") REFERENCES " + PHOTO_TABLE_NAME + "(" + PHOTO_ID + ")," +
            "FOREIGN KEY(" + PC_CLUE_ID + ") REFERENCES " + CLUE_TABLE_NAME + "(" + CLUE_ID + ")" +
            ");";

    // Photo to Bonus mapping table
    private static final String PB_TABLE_NAME = "pb";
    private static final String PB_PHOTO_ID = "photo_id";
    private static final String PB_BONUS_ID = "bonus_id";
    private static final String CREATE_PB_TABLE =
            "CREATE TABLE " + PB_TABLE_NAME + " (" +
            PB_PHOTO_ID + " INTEGER, " +
            PB_BONUS_ID + " INTEGER, " +
            "FOREIGN KEY(" + PB_PHOTO_ID + ") REFERENCES " + PHOTO_TABLE_NAME + "(" + PHOTO_ID + ")," +
            "FOREIGN KEY(" + PB_BONUS_ID + ") REFERENCES " + BONUS_TABLE_NAME + "(" + BONUS_ID + ")" +
            ");";

    private static final String TAGS_TABLE_NAME = "tags";
    private static final String TAGS_ID = "_id";
    private static final String TAGS_NAME = "tag";
    private static final String CREATE_TAGS_TABLE =
            "CREATE TABLE " + TAGS_TABLE_NAME + " (" +
            TAGS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TAGS_NAME + " TEXT " +
            ");";

    private static final String CT_TABLE_NAME = "Ct";
    private static final String CT_CLUE_ID = "clue_id";
    private static final String CT_TAG_ID = "tag_id";
    private static final String CREATE_PT_TABLE =
            "CREATE TABLE " + CT_TABLE_NAME + " (" +
            CT_CLUE_ID + " INTEGER, " +
            CT_TAG_ID + " INTEGER, " +
            "FOREIGN KEY(" + CT_CLUE_ID + ") REFERENCES " + CLUE_TABLE_NAME + "(" + CLUE_ID + ")," +
            "FOREIGN KEY(" + CT_TAG_ID + ") REFERENCES " + TAGS_TABLE_NAME + "(" + TAGS_ID + ")" +
            ");";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_CLUE_TABLE);
            db.execSQL(CREATE_BONUS_TABLE);
            db.execSQL(CREATE_PHOTO_TABLE);
            db.execSQL(CREATE_TAGS_TABLE);
            db.execSQL(CREATE_CB_TABLE);
            db.execSQL(CREATE_PB_TABLE);
            db.execSQL(CREATE_PC_TABLE);
            db.execSQL(CREATE_PT_TABLE);
            addTag(db, "All");
        } catch (Exception e) {
            Log.e(PhotohuntMainActivity.LOGNAME, "Could not create db");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // no need to upgrade yet...
    }

    private static String formInArgumentList(Cursor ids, int column) {
        if (ids.getCount() == 0) return "( )";
        String ret = "( ";
        ids.moveToFirst();

        while (!ids.isAfterLast()) {
            ret += ""+ids.getLong(column);
            ret += ", ";
            ids.moveToNext();
        }

        ret = ret.substring(0, ret.length() - 2);
        ret += " )";

        return ret;
    }

    public static Cursor getCluesCursor(SQLiteDatabase db) {
        return db.query(CLUE_TABLE_NAME, new String[] { CLUE_ID, CLUE_TEXT, CLUE_POINTS }, null, null, null, null, null);
    }

    public static Cursor getCluesCursor(SQLiteDatabase db, String tag) {
        Cursor c = db.query(TAGS_TABLE_NAME, new String[] { TAGS_ID }, TAGS_NAME + " = ? ", new String[] { tag }, null, null, null);
        long tagId = 0;
        if (c.moveToFirst())
            tagId = c.getLong(0);

        c = db.query(CT_TABLE_NAME, new String[] { CT_CLUE_ID }, CT_TAG_ID + " = ? ", new String[] { ""+tagId }, null, null, null);
        String ids = formInArgumentList(c, 0);
        return db.query(CLUE_TABLE_NAME, new String[] {CLUE_ID, CLUE_TEXT, CLUE_POINTS }, CLUE_ID + " IN " + ids, null, null, null, null);
    }

    public static Cursor getCluesCursor(SQLiteDatabase db, long clueId) {
        Cursor c = db.query(CB_TABLE_NAME, new String[] { CB_BONUS_ID }, CB_CLUE_ID + " = ? ", new String[]{ ""+clueId }, null, null, null);
        String ids = formInArgumentList(c, 0);
        return db.query(BONUS_TABLE_NAME, new String[] { BONUS_ID, BONUS_TEXT, BONUS_POINTS }, BONUS_ID + " IN " + ids, null, null, null, null);
    }

    public static Cursor getTagsCursor(SQLiteDatabase db) {
        return db.query(TAGS_TABLE_NAME, new String[] { TAGS_ID, TAGS_NAME }, null, null, null, null, null);
    }

    public static boolean addPhoto(SQLiteDatabase db, String path) {
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_PATH, path);
        cv.put(PHOTO_TIME, System.currentTimeMillis() / 1000);
        return db.insert(PHOTO_TABLE_NAME, null, cv) != -1;
    }

    public static boolean judgePhoto(SQLiteDatabase db, long id, boolean judge) {
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_JUDGE, judge ? 1 : 0);
        return
                db.update(PHOTO_TABLE_NAME, cv, PHOTO_ID + " = ?", new String[]{ ""+id }) != -1 &&
                modifyPhoto(db, id);
    }

    public static boolean isJudged(SQLiteDatabase db, long id) {
        Cursor c = db.query(PHOTO_TABLE_NAME, new String[] { PHOTO_JUDGE }, PHOTO_ID + " = ? ", new String[] { ""+id }, null, null, null);
        boolean ret = false;
        if (c.moveToFirst())
            ret = c.getInt(0) != 0;
        c.close();
        return ret;
    }

    public static boolean uploadPhoto(SQLiteDatabase db, long id) {
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_UPLOADED, System.currentTimeMillis()/1000);
        return db.update(PHOTO_TABLE_NAME, cv, PHOTO_ID + " = ?", new String[]{ ""+id }) != -1;
    }

    public static boolean modifyPhoto(SQLiteDatabase db, long id) {
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_MODIFIED, System.currentTimeMillis()/1000);
        return db.update(PHOTO_TABLE_NAME, cv, PHOTO_ID + " = ?", new String[]{ ""+id }) != -1;
    }

    public static boolean modifyNotes(SQLiteDatabase db, long id, String notes) {
        ContentValues cv = new ContentValues();
        cv.put(PHOTO_NOTE, notes);
        return
                db.update(PHOTO_TABLE_NAME, cv, PHOTO_ID + " = ?", new String[]{ ""+id }) != -1 &&
                modifyPhoto(db, id);
    }

    public static boolean addClueToPhoto(SQLiteDatabase db, long photo, long clue) {
        ContentValues cv = new ContentValues();
        cv.put(PC_CLUE_ID, clue);
        cv.put(PC_PHOTO_ID, photo);
        return
                db.insert(PC_TABLE_NAME, null, cv) != -1 &&
                modifyPhoto(db, photo);
    }

    public static boolean addBonusToPhoto(SQLiteDatabase db, long photo, long bonus) {
        ContentValues cv = new ContentValues();
        cv.put(PB_BONUS_ID, bonus);
        cv.put(PB_PHOTO_ID, photo);
        return
                db.insert(PB_TABLE_NAME, null, cv) != -1 &&
                modifyPhoto(db, photo);
    }

    public static long photoIdFromPath(SQLiteDatabase db, String path) {
        Log.d(PhotohuntMainActivity.LOGNAME, path);
        long ret =0 ;
        Cursor c = db.query(PHOTO_TABLE_NAME, new String[]{PHOTO_ID}, PHOTO_PATH + " = ?", new String[]{path}, null, null, null);
        if (c.moveToFirst())
             ret = c.getLong(0);
        c.close();
        return ret;
    }

    public static boolean addClue(SQLiteDatabase db, String name, long server, long points, String[] tags) {
        ContentValues cv = new ContentValues();
        cv.put(CLUE_POINTS, points);
        cv.put(CLUE_TEXT, name);
        cv.put(CLUE_SERVER_ID, server);
        if (db.insert(CLUE_TABLE_NAME, null,cv) == -1)
            return false;

        Cursor clueCursor = db.query(CLUE_TABLE_NAME, new String[] { CLUE_ID }, CLUE_TEXT + " = ?", new String[] { name }, null, null, null);
        if (!clueCursor.moveToFirst())
            return false;

        for (String tag : tags) {
            long id;
            if ((id = tagIdFromName(db, tag)) <= 0) {
                addTag(db, tag);
                id = tagIdFromName(db, tag);
                Log.d(PhotohuntMainActivity.LOGNAME, tag);
            }

            cv = new ContentValues();
            cv.put(CT_CLUE_ID, clueCursor.getLong(0));
            cv.put(CT_TAG_ID, id);
            if (db.insert(CT_TABLE_NAME, null, cv) == -1)
                return false;
        }

        // Let's add an "all" tag!
        long id;
        String tag = "All";
        if ((id = tagIdFromName(db, tag)) == -1) {
            addTag(db, tag);
            id = tagIdFromName(db, tag);
        }

        cv = new ContentValues();
        cv.put(CT_CLUE_ID, clueCursor.getLong(0));
        cv.put(CT_TAG_ID, id);

        clueCursor.close();
        return db.insert(CT_TABLE_NAME, null, cv) != -1;

    }

    public static boolean addClue(SQLiteDatabase db, String name, long server, long points, String[] tags, String[] bonusNames, long[] bonusServers, long[] bonusPointses) {
        if (!addClue(db, name, server, points, tags))
            return false;

        for (int i = 0; i < bonusNames.length; i++) {
            addBonus(db, bonusNames[i], bonusServers[i], bonusPointses[i]);
        }

        long[] bonusIds = translateFromServerClueIds(db, bonusServers, false);
        long[] clueIds = translateFromServerClueIds(db, new long[] { server }, true);
        for (long bonusId : bonusIds) {
            ContentValues cv = new ContentValues();
            cv.put(CB_BONUS_ID, bonusId);
            cv.put(CB_CLUE_ID, clueIds[0]);

            if (db.insert(CB_TABLE_NAME, null, cv) == -1) return false;
        }

        return true;
    }

    public static boolean addBonus(SQLiteDatabase db, String name, long server, long points) {
        ContentValues cv = new ContentValues();
        cv.put(BONUS_POINTS, points);
        cv.put(BONUS_SERVER_ID, server);
        cv.put(BONUS_TEXT, name);
        return db.insert(BONUS_TABLE_NAME, null, cv) != -1;
    }

    public static long[] translateFromServerClueIds(SQLiteDatabase db, long[] serverIds, boolean clues) {
        long[] ret = new long[serverIds.length];
        String args = "( ";
        String[] ids = new String[serverIds.length];
        Cursor c;

        int i = 0;
        for (long id : serverIds) {
            args += "?, ";
            ids[i++] = ""+id;
            Log.d(PhotohuntMainActivity.LOGNAME, "Id is: " + id);
        }

        args = args.substring(0, args.length() - 2);
        args += " )";
        if (clues)
            c = db.query(CLUE_TABLE_NAME, new String[]{CLUE_SERVER_ID}, CLUE_SERVER_ID + " IN " + args, ids, null, null, null);
        else
            c = db.query(BONUS_TABLE_NAME, new String[]{BONUS_SERVER_ID}, BONUS_SERVER_ID + " IN " + args, ids, null, null, null);

        c.moveToFirst();
        for (int j = 0; j < c.getCount(); j++) {
            ret[j] = c.getLong(0);
            c.moveToNext();
        }
        c.close();

        return ret;
    }

    public static String getNotes(SQLiteDatabase db, long id) {
        Cursor c = db.query(PHOTO_TABLE_NAME, new String[]{PHOTO_NOTE}, PHOTO_ID + " = ?", new String[]{"" + id}, null, null, null);
        String ret = "";
        if (c.moveToFirst())
            ret = c.getString(0);
        c.close();
        return ret;
    }

    public static long tagIdFromName(SQLiteDatabase db, String name) {
        Cursor c = db.query(TAGS_TABLE_NAME, new String[]{ TAGS_ID }, TAGS_NAME + " = ?", new String[]{ name }, null, null, null);
        if (c.moveToFirst())
            return c.getLong(0);
        c.close();
        return -1;
    }

    public static boolean addTag(SQLiteDatabase db, String name) {
        ContentValues cv = new ContentValues();
        cv.put(TAGS_NAME, name);
        return db.insert(TAGS_TABLE_NAME, null, cv) != -1;
    }

    public static long numJudgedPhotos(SQLiteDatabase db) {
        Cursor c = db.query(PHOTO_TABLE_NAME, new String[]{ PHOTO_ID }, PHOTO_JUDGE + " = 1", null, null, null, null);
        long ret = -1;
        if (c.moveToFirst())
            ret = c.getCount();
        c.close();

        return ret;
    }

    public static long numPhotos(SQLiteDatabase db) {
        Cursor c = db.query(PHOTO_TABLE_NAME, new String[]{ PHOTO_ID }, null, null, null, null, null);
        long ret = -1;
        if (c.moveToFirst())
            ret = c.getCount();
        c.close();

        return ret;
    }

    public static boolean hasBonuses(SQLiteDatabase db, long id) {
        Cursor c = db.query(CB_TABLE_NAME, new String[] { CB_BONUS_ID }, CB_CLUE_ID + " = ? ",new String[]{ ""+id }, null, null, null);
        boolean ret = c.getCount() > 0;
        c.close();
        return ret;
    }

    public static boolean deleteClueFromPhoto(SQLiteDatabase db, long photo, long clue) {
        return db.delete(PC_TABLE_NAME, PC_PHOTO_ID + "= ? and " + PC_CLUE_ID + " = ?", new String[] { ""+photo, ""+clue }) != -1 &&
                modifyPhoto(db, photo);
    }

    public static boolean deleteBonusFromPhoto(SQLiteDatabase db, long photo, long bonus) {
        return db.delete(PB_TABLE_NAME, PB_PHOTO_ID + "= ? and " + PB_BONUS_ID + " = ?", new String[] { ""+photo, ""+bonus }) != -1 &&
                modifyPhoto(db, photo);
    }

    public static ArrayList<Long> checkedClues(SQLiteDatabase db, long photoId) {
        Cursor c = db.query(PC_TABLE_NAME, new String[] { PC_CLUE_ID }, PC_PHOTO_ID + " = ? ", new String[] { ""+photoId }, null, null, null);
        ArrayList<Long> ret = new ArrayList<Long>(c.getCount());

        c.moveToFirst();
        while (!c.isAfterLast()) {
            ret.add(c.getLong(0));
            c.moveToNext();
        }
        c.close();

        return ret;
    }

    public static ArrayList<Long> checkedBonuses(SQLiteDatabase db, long photoId) {
        Cursor c = db.query(PB_TABLE_NAME, new String[] { PB_BONUS_ID }, PB_PHOTO_ID + " = ? ", new String[] { ""+photoId }, null, null, null);
        ArrayList<Long> ret = new ArrayList<Long>(c.getCount());

        c.moveToFirst();
        while (!c.isAfterLast()) {
            ret.add(c.getLong(0));
            c.moveToNext();
        }
        c.close();

        return ret;
    }

    public static Cursor getPhoto(SQLiteDatabase db, long photoId) {
        return db.query(PHOTO_TABLE_NAME, new String[] { PHOTO_JUDGE, PHOTO_NOTE, PHOTO_PATH }, PHOTO_ID + " = ? ", new String[] { ""+photoId }, null, null, null);
    }

    public static Cursor getCluesCursorForPhoto(SQLiteDatabase db, long photoId) {
        Cursor c = db.query(PC_TABLE_NAME, new String[] { PC_CLUE_ID }, PC_PHOTO_ID + " = ? ", new String[]{ ""+photoId }, null, null, null);
        String inArgs = formInArgumentList(c, 0);
        return db.query(CLUE_TABLE_NAME, new String[] { CLUE_SERVER_ID }, CLUE_ID + " IN " + inArgs, null, null, null, null);
    }

    public static ArrayList<Long> getBonusCursorForPhoto(SQLiteDatabase db, long photoId, long clueId) {
        ArrayList<Long> temp = new ArrayList<Long>();
        ArrayList<Long> ret = new ArrayList<Long>();

        Cursor c = db.query(PB_TABLE_NAME, new String[] { PB_BONUS_ID }, PB_PHOTO_ID + " = ? ", new String[]{ ""+photoId }, null, null, null);
        String inArgs = formInArgumentList(c, 0);
        c.close();
        c = db.query(BONUS_TABLE_NAME, new String[] { BONUS_SERVER_ID }, BONUS_ID + " IN " + inArgs, null, null, null, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            temp.add(c.getLong(0));
            c.moveToNext();
        }
        c.close();

        c = db.query(CB_TABLE_NAME, new String[] { CB_BONUS_ID }, CB_CLUE_ID + " = ? ", new String[] { ""+clueId }, null, null, null);
        c = translateToBonusServerIds(db, c);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (temp.contains(c.getLong(0)))
                ret.add(c.getLong(0));
            c.moveToNext();
        }
        c.close();

        return ret;
    }

    public static Cursor translateToBonusServerIds(SQLiteDatabase db, Cursor c) {
        String inArgs = formInArgumentList(c, 0);
        c.close();
        return db.query(BONUS_TABLE_NAME, new String[]{ BONUS_SERVER_ID }, BONUS_ID + " IN " + inArgs, null, null, null, null);
    }
}
